#pragma once
#include <iostream>
#include <functional>
class Array
{
	int *arr;
	int n;
	void copy(const Array&);
	void move(Array&&);
	Array apply(const Array&, const std::function<int(int, int)>&) const;
	friend std::ostream& operator<<(std::ostream&, const Array&);
	friend std::istream& operator>>(std::istream&, Array&);
public:
	int size() const;
	Array(int); //default
	Array(const Array&); //copy
	Array(Array&&); //move

	int& operator[](int);
	const int& operator[](int) const;
	Array& operator=(const Array&); //operator= copy
	Array& operator=(Array&&); //operator= move
	Array operator+(const Array&) const;
	Array operator-(const Array&) const;

	Array filter(std::function<bool(int)>) const;

	~Array(); //destructor

};