#include "Array.h"
#include <algorithm>


void Array::copy(const Array& other)
{
	n = other.n;
	arr = new int[n];
	for (int i = 0; i < n; ++i)arr[i] = other.arr[i];
}

void Array::move(Array&& other)
{
	n = std::move(other.n);
	arr = std::move(other.arr);
	other.arr = nullptr;
}

Array Array::apply(const Array& other, const std::function<int(int, int)>& f) const
{
	if (n != other.n)
		return Array(0);
	Array temp(n);
	for (int i = 0; i < n; ++i)
		temp[i] = f((*this)[i], other[i]);
	return temp;
}

int Array::size() const
{
	return n;
}

Array::Array(int n) :n(n), arr(new int[n]) //default
{}

Array::Array(const Array& other) //copy 
{
	copy(other);
}

Array::Array(Array&& other) //move
{
	move(std::move(other));
}

int& Array::operator[](int i)
{
	return arr[i];
}

const int & Array::operator[](int i) const
{
	return arr[i];
}

Array& Array::operator=(const Array& other) //operator= copy
{
	copy(other);
	return *this;
}

Array& Array::operator=(Array&& other) //operator= move
{
	move(std::move(other));
	return *this;
}

Array Array::operator+(const Array& other) const //operator+
{
	return apply(other, [](int x, int y) {return x + y; });
}

Array Array::operator-(const Array& other) const //operator-
{
	return apply(other, [](int x, int y) {return x - y; });
}

Array Array::filter(std::function<bool(int)> f) const
{
	int j = 0;
	for(int i=0;i<n;++i)
	{
	    if(f((*this)[i]))j++;
	}
	Array temp(j);
	j=0;
	for(int i=0;i<n;++i)
	{
	    if(f((*this)[i]))temp[j++]=(*this)[i];
	}
	return temp;
}

Array::~Array()
{
	delete[] arr;
	arr = nullptr;
}

std::ostream& operator<<(std::ostream& stream, const Array& array)
{
	std::for_each(array.arr, array.arr + array.n,

		[&stream](int i) {stream << i; }
		);
	return stream;
}

std::istream& operator>>(std::istream& stream, Array& array)
{
	for (int i = 0; i < array.n; ++i)
	{
		stream >> array[i];
	}
	return stream;
}
